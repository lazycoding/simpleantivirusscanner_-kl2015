#-------------------------------------------------
#
# Project created by QtCreator 2015-04-12T22:11:44
#
#-------------------------------------------------

QT       -= core gui

TARGET = ClamWrapper
TEMPLATE = lib
CONFIG += staticlib

SOURCES += clamwrapper.cpp

HEADERS += clamwrapper.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

unix|win32: LIBS += -L$$PWD/../ClamAV/lib/ -llibclamav

INCLUDEPATH += $$PWD/../ClamAV/include
DEPENDPATH += $$PWD/../ClamAV/include
