#include "clamwrapper.h"
#include <clamav.h>

static void msg_callback(...) { }

ClamWrapper::ClamWrapper()
{
    cl_set_clcb_msg((clcb_msg)&msg_callback);
    engine = NULL;
    dbLoaded = false;
    int ret;
    if ((ret = cl_init(CL_INIT_DEFAULT)) != CL_SUCCESS)
    {
        lastError = ErrorInitFail;
        errorString = cl_strerror(ret);
        return;
    }
    if (!(engine = cl_engine_new()))
    {
        lastError = ErrorInitFail;
        errorString = "Can't create new engine";
        return;
    }
    lastError = ErrorSuccess;
}

ClamWrapper::~ClamWrapper()
{
    if (engine != NULL)
        cl_engine_free(engine);
}

bool ClamWrapper::loadDatabase(unsigned int *signatureCount)
{
    if (lastError != ErrorSuccess || dbLoaded)
        return false;

    int ret;
    unsigned int sigs = 0;
    //cl_retdbdir()
    if ((ret = cl_load("database", engine, &sigs, CL_DB_STDOPT)) != CL_SUCCESS ||
        (ret = cl_engine_compile(engine)) != CL_SUCCESS)
    {
        lastError = ErrorLoadDatabaseFail;
        errorString = cl_strerror(ret);
        return false;
    }
    if (signatureCount != NULL)
        *signatureCount = sigs;
    lastError = ErrorSuccess;
    dbLoaded = true;
    return true;
}

bool ClamWrapper::scanFile(const char *fileName, const char **verdict, unsigned long *size)
{
    if (lastError != ErrorSuccess || !dbLoaded)
        return false;

    const char *_verdict = "";
    int ret = cl_scanfile(fileName, &_verdict, size, engine, CL_SCAN_STDOPT);
    if (verdict != NULL)
        *verdict = _verdict;

    if (ret == CL_VIRUS)
        return true;
    if (ret == CL_CLEAN)
        return false;

    lastError = ErrorScanFileFail;
    errorString = cl_strerror(ret);
    return false;
}
