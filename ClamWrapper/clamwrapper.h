#ifndef CLAMWRAPPER_H
#define CLAMWRAPPER_H

#include <stddef.h>

struct cl_engine;
class ClamWrapper
{
private:
    int lastError;
    const char *errorString;
    struct cl_engine *engine;
    bool dbLoaded;

public:
    ClamWrapper();
    ~ClamWrapper();
    bool loadDatabase(unsigned int *signatureCount=NULL);
    bool scanFile(const char *fileName, const char **verdict=NULL, unsigned long *size=NULL);
    int getLastError()
    {
        return lastError;
    }
    const char *lastErrorString()
    {
        return errorString;
    }

    enum
    {
        ErrorSuccess = 0,
        ErrorInitFail = 1,
        ErrorLoadDatabaseFail = 3,
        ErrorCannotOpenFile = 4,
        ErrorScanFileFail = 5
    };
};

#endif // CLAMWRAPPER_H
