#-------------------------------------------------
#
# Project created by QtCreator 2015-04-11T19:21:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleAV
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    scanner.cpp \
    fileinfo.cpp

HEADERS  += mainwindow.h \
    scanner.h \
    fileinfo.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../ClamWrapper/release/ -lClamWrapper
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../ClamWrapper/debug/ -lClamWrapper
else:unix: LIBS += -L$$OUT_PWD/../ClamWrapper/ -lClamWrapper

INCLUDEPATH += $$PWD/../ClamWrapper
DEPENDPATH += $$PWD/../ClamWrapper

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ClamWrapper/release/libClamWrapper.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ClamWrapper/debug/libClamWrapper.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ClamWrapper/release/ClamWrapper.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ClamWrapper/debug/ClamWrapper.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../ClamWrapper/libClamWrapper.a

unix|win32: LIBS += -L$$PWD/../ClamAV/lib/ -llibclamav
