#ifndef FILEINFO_H
#define FILEINFO_H

#include <QString>
class FileInfo
{
public:
    FileInfo();
    FileInfo(const FileInfo *info);
    QString verdict;
    QString fileName;
    bool isClean;
    unsigned long size;
};

#endif // FILEINFO_H
