#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDirIterator>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //bulkScanner = new BulkScanner(&scanner);
    QObject::connect(&scanner, SIGNAL(fileScanned(bool,const FileInfo*)),
                     this, SLOT(on_fileScanned(bool,const FileInfo*)));
    QObject::connect(&scanner, SIGNAL(dirScanned(bool,int,const QList<FileInfo>*)),
                     this, SLOT(on_dirScanned(bool,int,const QList<FileInfo>*)));
    QObject::connect(&scanner, SIGNAL(databaseLoaded(bool,ulong)),
                     this, SLOT(on_databaseLoaded(bool,ulong)));
    QObject::connect(&scanner, SIGNAL(write(QString)),
                     this, SLOT(on_write(QString)));
    QObject::connect(&scanner, SIGNAL(progressUpdated(int,int)),
                         this, SLOT(on_progressUpdated(int,int)));

    this->setFixedSize(this->geometry().width(),this->geometry().height());
    setUiEnabled(false);
    setProgressVisibility(false);
    scanner.loadDatabase();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_databaseLoaded(bool success, unsigned long signatureCount)
{
    if (success)
    {
        addToLog("Loaded " + QString::number(signatureCount) + " signatures\n");
        setUiEnabled(true);
    }
    else
    {
        QMessageBox::information(this, "Error", "Could not load the signature database.\nThis program will be terminated.");
        this->close();
    }
}

void MainWindow::on_fileScanned(bool success, const FileInfo *info)
{
    if (!success)
        logVerdict(info->fileName, "Error", false);
    else if (info->isClean)
        logVerdict(info->fileName, "Clean", true);
    else
        logVerdict(info->fileName, info->verdict, false);
}

void MainWindow::on_scanButton_clicked()
{
    QString *path = &(ui->pathLineEdit->text());
    if (QDir(*path).exists())
    {
        setProgressVisibility(true);
        setUiEnabled(false);
        scanner.scanDirectory(*path);
    }
    else if (QFile(*path).exists())
        scanner.scanFile(*path);
    else
        QMessageBox::information(this, "Error", *path + " is neither a valid file nor a valid folder.");
}

void MainWindow::on_write(QString str)
{
    addToLog("<font color=\"red\">" + str + "</font>");
}

void MainWindow::on_chooseFileButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open File"), "", tr("Files (*.*)"));
    ui->pathLineEdit->setText(fileName);
}

void MainWindow::on_chooseDirButton_clicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this,
        tr("Open File"), "");
    ui->pathLineEdit->setText(fileName);
}

void MainWindow::on_dirScanned(bool success, int totalFiles, const QList<FileInfo> *infectedFiles)
{
    setProgressVisibility(false);
    if (!success)
    {
        addToLog("Scan failed");
        return;
    }
    QString message = QString::number(totalFiles) + " files scanned. " + QString::number(infectedFiles->count()) + " infected files found";
    message += infectedFiles->count() > 0 ? ":" : ".";
    addToLog("");
    foreach (FileInfo file, *infectedFiles)
        logVerdict(file.fileName, file.verdict, false);

    addToLog(message);
    setUiEnabled(true);
}

void MainWindow::setUiEnabled(bool isEnabled)
{
    //ui->chooseDirButton->setEnabled(isEnabled);
    //ui->chooseFileButton->setEnabled(isEnabled);
    ui->scanButton->setEnabled(isEnabled);
    //ui->pathLineEdit->setEnabled(isEnabled);
}

void MainWindow::setProgressVisibility(bool isVisible)
{
    ui->progressBar->setVisible(isVisible);
    ui->progressLabel->setVisible(isVisible);
}

void MainWindow::on_progressUpdated(int filesTotal, int filesProcessed)
{
    ui->progressBar->setValue((int)(filesProcessed * 100.0 / filesTotal));
    ui->progressLabel->setText(QString::number(filesProcessed) + "/" + QString::number(filesTotal));
}

void MainWindow::logVerdict(QString path, QString verdict, bool isClean)
{
    QString s = path + ": ";
    s += "<font color=\"" + QString(isClean ? "green" : "red") + "\">" +
        verdict + "</font>";
    addToLog(s);
}

void MainWindow::addToLog(QString str)
{
    if (htmlLog.length() > 0)
        htmlLog = str + "<br>" + htmlLog;
    else
        htmlLog = str;
    ui->logTextEdit->setHtml(htmlLog);
}
