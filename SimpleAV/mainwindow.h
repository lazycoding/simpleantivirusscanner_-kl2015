#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "scanner.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_chooseFileButton_clicked();
    void on_databaseLoaded(bool success, unsigned long signatureCount);
    void on_fileScanned(bool success, const FileInfo *info);
    void on_progressUpdated(int filesProcessed, int filesTotal);
    void on_chooseDirButton_clicked();
    void on_dirScanned(bool success, int totalFiles, const QList<FileInfo> *infectedFiles);
    void on_scanButton_clicked();
    void on_write(QString str);

private:
    Ui::MainWindow *ui;
    Scanner scanner;
    QString htmlLog;
    //BulkScanner *bulkScanner;
    //bool batchOperationInProgress;
    void setUiEnabled(bool isEnabled);
    void setProgressVisibility(bool isVisible);
    void addToLog(QString str);
    void logVerdict(QString path, QString verdict, bool isClean);
};

#endif // MAINWINDOW_H
