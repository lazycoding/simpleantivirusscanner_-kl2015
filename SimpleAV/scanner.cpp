#include "scanner.h"
#include <QDirIterator>
Scanner::Scanner(QObject *parent) : QThread(parent)
{

}

Scanner::~Scanner()
{

}

bool Scanner::loadDatabase()
{
    if (isRunning())
        return false;

    currentAction = ActionLoadDatabase;
    start(LowPriority);
    return true;
}

bool Scanner::scanFile(const QString &fileName)
{
    if (isRunning())
        return false;

    currentAction = ActionScanFile;
    this->path = fileName;
    this->path.replace('/', '\\');
    start(LowPriority);
    return true;
}

bool Scanner::scanDirectory(const QString &dirPath)
{
    if (isRunning())
        return false;

    this->path = dirPath;
    currentAction = ActionScanDir;
    start(LowPriority);
    return true;
}

void Scanner::run()
{
    unsigned int signatureCount;
    bool result;
    const char *verdict;
    int totalFiles = 0;
    FileInfo info;

    QDirIterator *dirit;
    int processed;
    QList<QString> files;
    switch (currentAction) {
    case ActionLoadDatabase:
        result = clam.loadDatabase(&signatureCount);
        emit databaseLoaded(result, signatureCount);
        break;

    case ActionScanFile:
        info.isClean = true;
        info.fileName = path;
        info.size = 0;
        info.verdict = "";
        result = clam.scanFile(path.toStdString().c_str(), &verdict, &info.size);

        if (result)
        {
            info.verdict = verdict;
            info.isClean = false;
        }
        else if (clam.getLastError() == ClamWrapper::ErrorSuccess)
            result = true;
        else
            emit write("Error. " + path + ": " + QString(clam.lastErrorString()));

        lastFileInfo = info;
        emit fileScanned(result, &lastFileInfo);

        break;

    case ActionScanDir:
        processed = 0;
        if (!QDir(path).exists()){
            emit dirScanned(false, 0, 0);
            return;
        }
        dirit = new QDirIterator(path, QDirIterator::NoIteratorFlags | QDirIterator::Subdirectories);
        while (dirit->hasNext()){
            dirit->next();
            if (dirit->fileInfo().isFile())
                files.append(dirit->filePath());
        }
        totalFiles = files.count();
        lastInfectedFiles.clear();
        emit progressUpdated(totalFiles, processed);
        foreach (QString file, files) {
            file.replace('/', '\\');
            result = clam.scanFile(file.toStdString().c_str(), &verdict, &info.size);
            if (result)
            {
                info.verdict = verdict;
                info.isClean = false;
                info.fileName = file;
                lastInfectedFiles.append(FileInfo(info));
            }
            if (clam.getLastError() != ClamWrapper::ErrorSuccess)
                emit write("Error. " + file + ": " + QString(clam.lastErrorString()));
            emit progressUpdated(totalFiles, ++processed);
        }
        emit dirScanned(true, totalFiles, &lastInfectedFiles);
        break;
    default:
        break;
    }
}
