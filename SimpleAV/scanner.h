#ifndef SCANNER_H
#define SCANNER_H

#include <QObject>
#include <QThread>
#include <clamwrapper.h>
#include "fileinfo.h"

class Scanner : public QThread
{
    Q_OBJECT
public:
    explicit Scanner(QObject *parent = 0);
    ~Scanner();
    bool loadDatabase();
    bool scanFile(const QString &path);
    bool scanDirectory(const QString &path);
    const ClamWrapper &getWrapper()
    {
        return clam;
    }

private:
    ClamWrapper clam;
    QString path;
    FileInfo lastFileInfo;
    QList<FileInfo> lastInfectedFiles;
    enum ScannerAction
    {
        ActionUnknown,
        ActionLoadDatabase,
        ActionScanDir,
        ActionScanFile
    } currentAction;

protected:
    void run();

signals:
    void databaseLoaded(bool success, unsigned long signatureCount);
    void fileScanned(bool success, const FileInfo *info);
    void dirScanned(bool success, int totalFiles, const QList<FileInfo> *infectedFiles);
    void progressUpdated(int total, int completed);
    void write(QString str);
public slots:
};

#endif // SCANNER_H
