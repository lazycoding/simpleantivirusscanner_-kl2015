#include <stdio.h>
#include <clamwrapper.h>

int main(int argc, char **argv)
{
    unsigned long int size = 0;
    unsigned int sigs = 0;
    const char *virname = "";

    if(argc != 2) {
        printf("Usage: %s file\n", argv[0]);
        return 2;
    }

    ClamWrapper clam;
    clam.loadDatabase(&sigs);
    printf("Loaded %u signatures.\n", sigs);

    clam.scanFile(argv[1], &virname, &size);
    printf("Verdicts:\n\t%s (%u): %s.\n", argv[1], size, virname);

    return 0;
}
